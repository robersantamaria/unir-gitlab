# Repo para EIEC - DevOps - UNIR

Este repositorio incluye un proyecto sencillo para demostrar el uso de GitLab y GitLab CI/CD. El objetivo es que el alumno pueda usar GitLab CI/CD rápidamente, por lo que el código y la estructura del proyecto son especialmente sencillos. Salvo por el pipeline definido en `.gitlab-ci.yaml`, el código es igual al de https://github.com/srayuso/unir-test. El repositorio https://github.com/srayuso/unir-build-tools contiene comandos para facilitar el despliegue de GitLab en local, pero se recomienda el uso de la versión SaaS.
